require 'test_helper'

class TangoListsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get tango_lists_create_url
    assert_response :success
  end

  test "should get destroy" do
    get tango_lists_destroy_url
    assert_response :success
  end

  test "should get index" do
    get tango_lists_index_url
    assert_response :success
  end

end
