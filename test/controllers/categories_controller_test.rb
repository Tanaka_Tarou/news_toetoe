require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get culture" do
    get categories_culture_url
    assert_response :success
  end

  test "should get nature" do
    get categories_nature_url
    assert_response :success
  end

  test "should get science" do
    get categories_science_url
    assert_response :success
  end

  test "should get mind-body" do
    get categories_mind-body_url
    assert_response :success
  end

  test "should get gov-jus" do
    get categories_gov-jus_url
    assert_response :success
  end

  test "should get human" do
    get categories_human_url
    assert_response :success
  end

end
