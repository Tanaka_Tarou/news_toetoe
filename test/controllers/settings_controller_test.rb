require 'test_helper'

class SettingsControllerTest < ActionDispatch::IntegrationTest
  test "should get profile" do
    get settings_profile_url
    assert_response :success
  end

  test "should get email" do
    get settings_email_url
    assert_response :success
  end

  test "should get password" do
    get settings_password_url
    assert_response :success
  end

end
