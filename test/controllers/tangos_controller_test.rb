require 'test_helper'

class TangosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tango = tangos(:one)
  end

  test "should get index" do
    get tangos_url
    assert_response :success
  end

  test "should get new" do
    get new_tango_url
    assert_response :success
  end

  test "should create tango" do
    assert_difference('Tango.count') do
      post tangos_url, params: { tango: {  } }
    end

    assert_redirected_to tango_url(Tango.last)
  end

  test "should show tango" do
    get tango_url(@tango)
    assert_response :success
  end

  test "should get edit" do
    get edit_tango_url(@tango)
    assert_response :success
  end

  test "should update tango" do
    patch tango_url(@tango), params: { tango: {  } }
    assert_redirected_to tango_url(@tango)
  end

  test "should destroy tango" do
    assert_difference('Tango.count', -1) do
      delete tango_url(@tango)
    end

    assert_redirected_to tangos_url
  end
end
