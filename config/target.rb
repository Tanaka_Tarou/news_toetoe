SCIAME = "https://www.scientificamerican.com/section/lateststories/"
SCIAME_TARGET_URLS_XML =  '//article/div/h2/a'
require 'anemone'
require 'csv'


def get_wanteds
  wanteds = []
  csv_data = CSV.read(Rails.root.join('app', 'controllers', 'wanteds.csv'))
  #開発環境ではこちらをしよう
  
  csv_data.each do |word|
    wanteds.push(word)
  end
  return wanteds
end

def count_word(txt)
  return txt.downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\:|…|\s\-\s|\-{2,}/ , "").split(" ").length
end  

def hikaku(targets)
  #重要単語の配列を取得
  wanteds = get_wanteds
  #カウンター
  count = 0
  #抽出した単語を入れる配列
  daiji_words = {}
  
  #記事の単語と用意した単語を比較して daiji_words にいれる
  targets.each do |target|
    wanteds.each do |wanted|
     if wanted[0] == target
       #daiji_words["vary"] = "～をかえる" => {"vary"=>"～をかえる"}
       daiji_words[wanted[0]] =  wanted[1]
     end
   end
  end
  return daiji_words
end


# SCIAME のindex のリンクを渡すことでURLをもってくる
def get_links
  target_links = []
  
  ######### SCIAME 固有 #################
  doc = Nokogiri::HTML(open(SCIAME))
  doc.xpath(SCIAME_TARGET_URLS_XML).each do |link|
    target_links.push(link[:href].to_s) 
  end
  ######################################
  return target_links.grep %r(article)
end



def get_target(*target_links)
  urls    = []
  results = []
  counter = 0
  lem = Lemmatizer.new
  
  # Science American のリンクを取得
  target_links = get_links
  target_links.each do |target_link|
    urls.push(target_link)
  end
  
  Anemone.crawl(urls, :depth_limit => 0, :delay=>1) do |anemone|
    anemone.on_every_page do |page|
      
      hash = Hash.new{|h,k| h[k]=[]}
      doc  = Nokogiri::HTML.parse(page.body)
      
      
      #########この範囲がsciame 固有#############
   
      title = doc.xpath("//main/header/div/h1").text
      preface = doc.xpath("//main/header/div/p").text
      body  = doc.xpath('//main/div[2]/div[2]/div/section/div/div/div/p').text
      sitename = doc.xpath('//header/div[2]/a').text
      
      ###########################################
      word_count = count_word(body) 
      full_txt = (title + preface + body).downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\:|…|\s\-\s|\-{2,}/ , "")
                  .split(" ").uniq #.sort 
      full_txt.map! {|item| lem.lemma(item)}
      
      url   = urls[counter]
      
      if hikaku(full_txt).length >= 40
        pic = doc.at('//main/div[2]/div[2]/div/section/figure[1]/div//img').attribute('src').to_s

        hash[:daiji_words] = hikaku(full_txt.uniq)
        hash[:title] = title
        hash[:body]  = body[0..200]
        hash[:sitename] = sitename
        hash[:url]   = url
        hash[:pic]   = pic
        hash[:count] = word_count
        hash[:tag]   = hikaku(full_txt.uniq.sort).map{|k,v| k}
        #p hash
        #puts "-------------------"
        results.push(hash)
      end
      counter += 1
    end
  end
  return results
end

end
