require 'clockwork'
require File.expand_path('../boot', __FILE__)
require File.expand_path('../environment', __FILE__)


module Clockwork
  handler do |job|
    puts "Running #{job}"
  end
  
  #new.perform の引数を history か sciame にして収集サイトを変更。
  every(20.seconds, 'shushu') { Delayed::Job.enqueue CreateArticleJob.new.perform("history")}
end