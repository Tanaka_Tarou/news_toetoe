Rails.application.routes.draw do
  
  
  get 'categories/culture'
  get 'categories/nature'
  get 'categories/science'
  get 'categories/mind_body'
  get 'categories/gov_jus'
  get 'categories/human'

  get 'notice/index'

  get   'settings/profile'
  patch 'settings/profile'  => 'settings#profile_update'
  get   'settings/email'  
  patch 'settings/email'    => "settings#email_update" 
  get   'settings/password'
  patch 'settings/password' => 'settings#password_update'
  
  
  get 'sessions/new'

  root 'articles#index'
  get  'static_pages/home'
  get  'static_pages/help'
  get  '/signup',  to: 'users#new'
  get  '/login',   to: 'sessions#new'
  post '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'
  post '/tangos/search', to: 'tangos#search'
  post 'tango_lists/search', to: 'tango_lists#search'

  
  resources :users do
    resources :tango_lists, only: [:index]
    member do
      get :following, :followers
    end
  end
  
  resources :articles do
    get :autocomplete_tango_word, :on => :collection    
    resources :picks do
      resources :likes, only: [:create, :destroy]
    end
  end
  
  resources :relationships,  only: [:create, :destroy]
  resources :tangos do
    resources :tango_lists, only: [:create, :destroy]
  end
end
