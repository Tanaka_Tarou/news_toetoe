class TangoListsController < ApplicationController
  before_action :set_user, only: [:index]
  before_action :logged_in_user?
  
  def search
    @tangos = current_user.my_lists.search(params[:q])
  end
  
  def create
    @tango_id = params[:tango_id]    #ajax でつかう
    current_user.tango_lists.create(tango_id: @tango_id)
  end


  def destroy
    @tango_list = current_user.tango_lists.find(params[:id])
    @tango_list.destroy
    @tango_id = params[:tango_id] #ajax で使う
  end

  def index
    @tangos = @user.my_lists
  end
  
  private
    def set_tango
      #@tango = Tango.find(params[:tango_id])
    end
    
    def set_user
      @user = User.find(params[:user_id]) || current_user
    end
end
