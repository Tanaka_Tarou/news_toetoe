class LikesController < ApplicationController
  before_action :logged_in_user?, only: [:create, :destroy]
  before_action :correct_like_user?, only: :destroy
  
  
  def create
    @like = current_user.likes.build(pick_id: params[:pick_id])
    @like.save
    
    @pick = Pick.find(params[:pick_id])
    @pick_user = User.find(params[:pick_user_id])
    @article_id = params[:article_id]
    
    # current_userがpick_userをいいねしました。（記事のタイトル）というのを作成する
    Notice.create_notice(@pick_user.id, current_user.id, "like", @article_id)
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end

  def destroy
    @like = current_user.likes.find_by(pick_id: params[:pick_id])
    @like.destroy
    @pick = Pick.find(params[:pick_id])
    @pick_user = User.find(params[:pick_user_id])
    @article_id = params[:article_id]
    
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end
  
  private
    def correct_like_user?
      @like = current_user.likes.find_by(id: params[:id])
      redirect_to root_url if @like.nil?
    end
end
