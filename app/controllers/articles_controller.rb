class ArticlesController < ApplicationController
  include ArticlesHelper
  
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  autocomplete :tango, :word
  before_action :admin_user?, except: [:index, :show]

  

  def index
    @articles = Article.all.search(params[:search]) #.limit(10)
  end

  def show
    if logged_in?
      @pick = current_user.picks.find_by(article_id: @article.id) || Pick.new
    end
    @now  = Time.zone.now
    @new_articles = Article.order(created_at: :desc).limit(10)
    @past = @article.created_at 
    @dif_time = @past - @now
  end

  def new
    @article  = Article.new 
    @articles = Article.select(:category).distinct
  end

  def edit
  end

  def create
    @article = Article.new(article_params)
    title = params[:article][:title]
    body  = params[:article][:body]
    full_text = get_full_text(title.to_s, body.to_s)
    @article.daiji_words = hikaku(full_text)
    @article.body  = body.to_s.gsub(/\t|\n/, "")[0, 200]
    @article.tag   = hikaku(full_text.uniq.sort).map{|k,v| k}
    @article.count = count_word(body.to_s)
    @article.save
    redirect_to 'index'
  end

  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end
  

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title,:body, :site_name, :url, :shudopic, :category, :post_time)
    end


end