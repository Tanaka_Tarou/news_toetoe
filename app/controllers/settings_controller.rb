class SettingsController < ApplicationController
  before_action :set_user
  before_action :logged_in_user?
  
  
  def profile
  end
  
  def profile_update
    respond_to do |format|

      if current_user.update_attributes(profile_params)
        format.html do
          redirect_to '/settings/profile'
          flash[:notice] = "更新できました"
        end
        format.js
      else
        format.html do 
          render 'settings/profile'
          flash[:danger] = "保存できませんでした。入力内容を確認してください"
        end
        format.js
      end   
    end
  end

  def email
    @email ||= current_user.email 
  end
  
  def email_update
    if current_user.update(email_params)
      redirect_to '/settings/email'
      flash[:notice] = "更新できました"
    else
      @email = current_user.attribute_was(:email)  
      render '/settings/email'
    end    
  end
  

  def password
  end
  
  def password_update
    if current_user.update(password_params)
      redirect_to '/settings/password'
      flash[:notice] = "更新できました"
    else
      render '/settings/password'
      flash[:danger] = "保存できませんでした。入力内容を確認してください" 
    end        
  end
  
  private
    
    def set_user
      @user = current_user
    end
  
    def email_params
      params.require(:user).permit(:email)
    end
    
    def profile_params
      params.require(:user).permit(:name, :hitokoto, :user_pic)
    end
    
    def password_params
      params.require(:user).permit(:password, :password_confirmation)
    end
end
