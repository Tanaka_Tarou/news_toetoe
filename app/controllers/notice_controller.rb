class NoticeController < ApplicationController
  before_action :logged_in_user?, only: :index
  def index
    @notices = current_user.notices
    
    #read_flag をtrue にして既読にする作業
    unread_notices = current_user.notices.where(read_flag: false)
    unread_notices.update_all(read_flag: true)
  end
end
