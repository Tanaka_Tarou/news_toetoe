class CategoriesController < ApplicationController
  def culture
    @articles = Article.where(category: "Culture").limit(9)
  end

  def nature
    @articles = Article.where(category: "Nature").limit(9)
  end

  def science
    @articles = Article.where(category: "Science").limit(9)
  end

  def mind_body
    @articles = Article.where(category: "mind-body").limit(9)
  end

  def gov_jus
    @articles = Article.where(category: "gov-jus").limit(9)
  end

  def human
    @articles = Article.where(category: "human").limit(9)
  end
end
