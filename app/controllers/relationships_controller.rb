class RelationshipsController < ApplicationController
  before_action :logged_in_user?
  
  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    Notice.create_notice(@user.id, current_user.id, "follow")
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end  
  end
  
  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end
end
