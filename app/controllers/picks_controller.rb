class PicksController < ApplicationController
  before_action :logged_in_user?
  
  def create
    pick = current_user.picks.build do |p|
      p.article_id = params[:article_id]
      p.comment    = params[:pick][:comment] if params[:pick][:comment]
    end
    if pick.save
      flash.now[:notice] = "pick!"
      redirect_to article_path(params[:article_id])
    else
      flash.now[:danger] = "pick 出来ませんでした。"
      redirect_to article_path(params[:article_id]), notice: "Pickしました"
    end
  end
  
  def update
    @pick = Pick.find(params[:id])
    @pick.comment = params[:pick][:comment] if params[:pick][:comment]
    
    if @pick.save
      flash.now[:notice] = "pick!"
      redirect_to article_path(params[:article_id])
    else
      flash.now[:danger] = "pick 出来ませんでした。"
      redirect_to article_path(params[:article_id]), notice: "Pickしました"
    end

  end

  def destroy
    pick = current_user.picks.find_by(article_id: params[:article_id])
    pick.destroy!
    redirect_to article_path(params[:article_id]), notice: "Pickを解除しました"
  end
end
