class TangosController < ApplicationController
  before_action :set_tango, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user?, except: [:index, :search]
  before_action :admin_user?, except: [:search, :index, :show]
  

  def search
    @tangos = Tango.search(params[:q])
  end

  def index
    @tangos = Tango.all[10..20] 
    #binding.pry
  end

  # GET /tangos/1s
  # GET /tangos/1.json
  def show
  end

  # GET /tangos/new
  def new
    @tango = Tango.new
  end

  # GET /tangos/1/edit
  def edit
  end

  def create
    @tango = Tango.new(tango_params)

    respond_to do |format|
      if @tango.save
        format.html { redirect_to @tango, notice: 'Tango was successfully created.' }
        format.json { render :show, status: :created, location: @tango }
      else
        format.html { render :new }
        format.json { render json: @tango.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @tango.update(tango_params)
        format.html { redirect_to @tango, notice: 'Tango was successfully updated.' }
        format.json { render :show, status: :ok, location: @tango }
      else
        format.html { render :edit }
        format.json { render json: @tango.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tangos/1
  # DELETE /tangos/1.json
  def destroy
    @tango.destroy
    redirect_to tangos_url
    flash[:notice] = 'Tango was successfully destroyed.' 
  end
  
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tango
      @tango = Tango.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tango_params
      params.fetch(:tango, {})
    end
    
end
