module ArticlesHelper
  
  ONE_DAY  = 86400
  ONE_HOUR = 3600
  
  # index で○○時間前と表示する
  def time_ago(past)
    now  = Time.zone.now
    dif_time = (now - past).to_i
    puts dif_time

    if dif_time < ONE_HOUR
      return ( (dif_time/60).to_s + "分前")
    elsif  ONE_HOUR <= dif_time && dif_time <= ONE_DAY
      return ((dif_time/60/60).to_s + "時間前")
    else 
      return past.year.to_s + "/" + past.month.to_s + "/" + past.day.to_s  
    end
  end
  
  #article用の画像。clockで取得したら:pic へ、手動でいれたら :shudoupic に入るからそのため。
  def image_tag_for_article(article)
    if !article.pic.nil?
      image_tag article.pic, size: "450x300"
    elsif !article.shudopic.nil?
      image_tag article.shudopic, class:"show-div"
    else
      image_tag 'default.jpg'
    end
  end

  #txt から単語数をかぞえる
  def count_word(txt)
    return txt.downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\:|…|\s\-\s|\-{2,}/ , "").split(" ").length
  end  


  #csv から重要単語の配列を拾ってくる
  def get_wanteds
    wanteds = []
    csv_data = CSV.read(Rails.root.join('app', 'assets', 'csvs','wanteds.csv'))
    csv_data.each do |word|
      wanteds.push(word)
    end
    return wanteds
  end
  
  def get_full_text(title, body)
    lem = Lemmatizer.new
    full_txt = (title + body).downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\r|\n|\:|…|\s\-\s|\-{2,}/ , "")
                .split(" ").uniq 
    full_txt.map! {|item| lem.lemma(item)}
  end
  
  
  def hikaku(targets)
    #重要単語の配列を取得
    wanteds = get_wanteds
    #カウンター
    count = 0
    #抽出した単語を入れる配列
    daiji_words = {}
  
    #記事の単語と用意した単語を比較して daiji_words というハッシュにいれる
    targets.each do |target|
      wanteds.each do |wanted|
        if wanted[0] == target
          #daiji_words["vary"] = "～をかえる" => {"vary"=>"～をかえる"}
          daiji_words[wanted[0]] =  wanted[1]
        end
      end
    end
    return daiji_words
  end

end
