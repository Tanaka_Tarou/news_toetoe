module ApplicationHelper
  def image_tag_with_default(pic)
    if pic.nil?
      image_tag('default.jpg')
    else
      image_tag pic
    end
  end
  
  
  
end
