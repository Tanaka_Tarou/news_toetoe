module SessionsHelper
  
  def log_in(user)
    session[:user_id] = user.id
  end
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  def logged_in?
    !current_user.nil?
  end
  
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end  
  
  def logged_in_user?
    store_location
    flash[:danger] = "ログインを済ませてください"
    redirect_to login_path
  end
  

  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
  
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  def admin_user?
    redirect_to root_path if current_user.nil?
    redirect_to root_path if current_user.admin?
  end
end
