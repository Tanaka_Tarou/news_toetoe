class Article < ApplicationRecord
  has_many :notices, dependent: :destroy
  has_many :picks, dependent: :destroy
  default_scope -> { order(post_time: :desc) }
  scope :culture, ->{where(category: "Culture")}
  scope :nature,  ->{where(category: "Nature")}
  scope :science,  ->{where(category: "Science")}
  scope :mind_body,  ->{where(category: "Mind & Body")}
  scope :gov_jus,  ->{where(category: "Government & Justice")}
  scope :human,  ->{where(category: "Human")}

  mount_uploader :shudopic, ArticlePicUploader
  require 'csv'
  require 'open-uri'

  # article Article.new
  # article.get_target("サイト名")で全部取得できる。 sciame とか mnt とか
  
  #単語リストを配列にして吐き出す
  def self.get_tags
    tags = []
    csv_data = CSV.read(Rails.root.join('app', 'assets', 'csvs','wanteds.csv'))
    csv_data.each do |word|
      tags.push(word[0])
    end
    return tags.to_s
  end



  #検索機能。index のところで呼び出される。検索ワードがなければ全部の記事
  def self.search(search)
    search.strip! if search
    if search == ""
      Article.all              #空欄なら全部
    elsif search  
      # \" のおかげで "" で囲まれてる "dog" だけをひろい "hotdog" を拾わない
      Article.where(['tag LIKE ?',  "%\"#{search}\"%"])
    else
      Article.all
    end
  end

  
  #記事のカテゴリーを用意したカテゴリに合わせる
  def get_categories(target_category, site_name=ScientificAmerican)
    result = ""
    categories = []
    #csvファイルを開く。 category_sciame.csv にデータをいれてある。
    csv_data = CSV.read(Rails.root.join('app', 'assets', 'csvs', 'category', "category_#{site_name}.csv"))
  
    #csvのデータをハッシュにいれる
    csv_data.each do |word|
      categories.push(word)
    end
    
    #記事のカテゴリを自サイトのカテゴリに合わせる
    categories.each do |category|
      result =  category[1] if category[0] == target_category
    end
    return  result
  end

  
  #一覧ページのリンクを取得する。get_targetで呼び出される
  def get_links 
    target_links = []
  
    doc = Nokogiri::HTML(open(@idx_link))
    doc.xpath(@each_link).each do |link|
      target_links.push( @httpwww + link[:href].to_s) 
    end
    return target_links
  end  
  
  #csv から重要単語の配列を拾ってくる
  def get_wanteds
    wanteds = []
    csv_data = CSV.read(Rails.root.join('app', 'assets', 'csvs','wanteds.csv'))
    csv_data.each do |word|
      wanteds.push(word)
    end
    return wanteds
  end
  
  
  
  
  #txt から単語数をかぞえる
  def count_word(txt)
    return txt.downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\:|…|\s\-\s|\-{2,}/ , "").split(" ").length
  end  
  
  
  def hikaku(targets)
    #重要単語の配列を取得
    wanteds = get_wanteds
    #カウンター
    count = 0
    #抽出した単語を入れる配列
    daiji_words = {}
  
    #記事の単語と用意した単語を比較して daiji_words というハッシュにいれる
    targets.each do |target|
      wanteds.each do |wanted|
        if wanted[0] == target
          #daiji_words["vary"] = "～をかえる" => {"vary"=>"～をかえる"}
          daiji_words[wanted[0]] =  wanted[1]
        end
      end
    end
    return daiji_words
  end
  
  def get_target(site_name)
    urls    = []
    results = []
    counter = 0
    lem = Lemmatizer.new
    
    case site_name
      when "sciame"
        @idx_link  = "https://www.scientificamerican.com/section/lateststories/"
        @each_link = '//article/div/h2/a' 
        @httpwww   = ""
        
        @title     = "//main/header/div/h1"
        @preface   = "//main/header/div/p"
        @body      = '//main/div[2]/div[2]/div/section/div/div/div/p'
        @pic       = '//main/div[2]/div[2]/div/section/figure[1]/div//img'
        @category  =  '//header/div/div/a'
        @sitename  = "Scientific American"


      when "history"
        @idx_link  = "http://www.history.com/news"
        @each_link = "//body/div[2]/div/div/div/div[1]/div/div/strong/a"
        @httpwww   = "http://www.history.com"
        
        @title     = "//body/div[2]//div/h1[@class = 'title']"
        @preface   = "//article/p"
        @body      = "//article/div[2]/p"
        @pic       = '//body/div[2]//img'
        @category  = "Culture"
        @sitename  = "History"

        
      when "mnt"
        @idx_link  = "http://www.medicalnewstoday.com/"
        @each_link = "//body/div[3]/div/div[1]/div/div/ul/li/a"
        @httpwww   = "http://www.medicalnewstoday.com"
        
        @title     = "//article/div[1]/h1"
        @preface   = "//article/div[2]/div[5]/div/header"
        @body      = "//article/div[2]/div[5]/div"
        @sitename  = "Medical News Today"
        @pic       = "//article/div[2]/div[5]//div[@class='photobox_right']//img"
    end    
    
    
    # index ページからリンクを取得
    target_links = get_links
    target_links.each do |target_link|
      urls.push(target_link)
    end
  
    Anemone.crawl(urls, :depth_limit => 0, :delay=>1) do |anemone|
      anemone.skip_links_like /\/podcast\/|\/video\//
      anemone.on_every_page do |page|
        hash = Hash.new{|h,k| h[k]=[]}
        doc  = Nokogiri::HTML.parse(page.body)
        
        #サイトによってリンクが格納されてるセレクタが異なる。pic はhttps://www.natiogeo～.jpgみたいな形式に。
        if @sitename == "Medical News Today"
          pic = doc.at(@pic).attribute('data-src').to_s
        elsif doc.at(@pic)
          pic = doc.at(@pic).attribute('src').to_s
        end
        
        #カテゴリをつける。historyは強制culture
        if @sitename == "History"
          category = @category
        else
          target_category = doc.xpath(@category.to_s).text
          category = get_categories(target_category, @sitename.gsub(" ", ""))
        end
        
        #インスタンス変数は上の switch で取得  
        sitename = @sitename
        url   = urls[counter]
        title = doc.xpath(@title).text
        preface = doc.xpath(@preface).text
        body  = doc.xpath(@body).text
        word_count = count_word(body) 

        #1時間単位で適当に足すことでランダム投稿機能をつけてる
        post_time  = Time.zone.now + 60*60*(rand(10)+1)
        

        #######画像ダウンロード
        kakuchosi = File.basename(pic).match(/((\..*)(?=\?))|(\..*)/)
        day      = post_time.strftime("%Y%m%d%H%M")
        fileName = title.gsub(" ", "_").gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\r|\n|\:|…|\s\-\s|\-{2,}|\[|\]/ , "")         #picの名前を数字のハッシュに変えた
        dirName  = "app/assets/images/"      #asset/imagesに保存する
        filePath = dirName + day.to_s + "_" + fileName + kakuchosi.to_s 
        
        open(filePath, 'wb') do |output|     #保存するパス名。画像の名前まで書くと画像ファイルが作られる
          open(pic) do |data|                #保存したい画像のリンク
            output.write(data.read) 
          end
        end
        ######################        
        
        full_txt = (title + preface + body).downcase.gsub(/\(|\)|\.|\d+|\,|\!|\?|\"|“|”|\r|\n|\:|…|\s\-\s|\-{2,}/ , "")
                    .split(" ").uniq 
        full_txt.map! {|item| lem.lemma(item)}
      
        hash[:daiji_words] = hikaku(full_txt.uniq)
        hash[:title] = title
        hash[:body]  = preface.gsub(/\t|\n/, "")[0, 200]
        hash[:url]   = url
        hash[:pic]   =  day.to_s + "_" + fileName + kakuchosi.to_s 
        hash[:tag]   = hikaku(full_txt.uniq.sort).map{|k,v| k} #英単語の部分だけ
        hash[:site_name] = sitename
        hash[:count] = word_count
        hash[:category]  = category
        hash[:post_time] = post_time
      
        results.push(hash)
        counter += 1
        
        
      end
    end
  return results
  end


#  default_scope { where('post_time >= ?', Time.zone.now) }
=begin
  validates :daiji_words, presence: true
  validates :sitename,    presence: true,
                          length: {maximum:200, minimum: 1}
  validates :title, presence: true,
                    length: {maximum: 1000, minimum: 10}
  validates :body,  presence: true,
                    length: {maximum: 301, minimum: 10}
  #validates :count, presence: true,
  #                  length: {maximum: 2000, minimum: 30}
  validates :url,   uniqueness: true,
                    format: URI::regexp(%w(http https))
  validate  :picture_size

=end

  private
    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:shudopic, "should be less than 5MB")
      end
    end
end
