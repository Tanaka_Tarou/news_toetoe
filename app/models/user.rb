class User < ApplicationRecord
  attr_accessor :remember_token  
  
  #config.autoload_paths += Dir[Rails.root.join('app', 'uploaders')]  
  has_many :tango_lists, dependent: :destroy
  has_many :my_lists, through: :tango_lists, source: :tango  
  has_many :notices, dependent: :destroy
  has_many :likes
  has_many :picks, dependent: :destroy
  has_many :active_relationships, class_name: "Relationship",
                                  foreign_key: "follower_id",
                                  dependent: :destroy
  has_many :passive_relationships, class_name: "Relationship",
                                   foreign_key: "followed_id",
                                   dependent: :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_secure_password
  mount_uploader :user_pic, PictureUploader
  before_save { self.email = email.downcase }
  validates :agreement, acceptance: true
  validates :name, presence: true, length: { maximum: 50 }
  validates :hitokoto, length: {maximum: 500}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password  
  validates :password, length: { minimum: 6 }, allow_blank: true
  validate  :picture_size

  
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # ランダムなトークンを返す
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # ユーザーを永続的セッションに記憶する
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # 渡されたトークンがダイジェストと一致したらtrueを返す  
  def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end
  
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user).destroy
  end
  
  def following?(other_user)
    !following.find_by(id: other_user.id).nil?
  end
  
  #自分とフォロイングのピックした記事を取得
  def timeline
    following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id"
    following_and_my_pick = Pick.where("user_id IN (#{following_ids}) OR user_id = :user_id",user_id: id)
    Article.find(following_and_my_pick.map(&:article_id))
  end
  
  #プロフィール画面のユーザーがピックしている記事一覧
  def mypicks
    picks = Pick.where("user_id = ?", id)
    Article.find(picks.map(&:article_id))
  end
  
  private
    def picture_size
      if user_pic.size > 5.megabytes
        errors.add(:user_pic, "5MB より小さい要領にしてください")
      end
    end
  
    
end

#following_ids = "SELECT followed_id FROM relationships WHERE followed_id = :user_id"
#pick = Pick.where("user_id IN (#{following_ids}) OR user_id = :user_id", user_id: id)
