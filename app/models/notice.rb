class Notice < ApplicationRecord
  belongs_to :user
  belongs_to :article
  
  scope :unreads, -> {where(read_flag: false)}
  default_scope {order(created_at: :desc)}

#お知らせ機能。フォローの場合を考慮して article_id は nil で。
# followアクションと likeアクションで呼び出される
  def self.create_notice(user_id, aiteno_id, type, article_id=nil)
    aite    = User.find(aiteno_id).name
    article_title = Article.find(article_id).title if !article_id.nil?
    message =
      case type
      when "like"
        "#{aite}さんがあなたのPickにいいねをしました。#{article_title}"[0..70]      
      when "follow" 
        "#{aite}さんがあなたをフォローしました"
      else
        nil
      end
    Notice.create(user_id: user_id,
                  aiteno_id:  aiteno_id,
                  message:    message,
                  article_id: article_id)
  end
end