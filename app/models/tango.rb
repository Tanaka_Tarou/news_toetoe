class Tango < ApplicationRecord
  has_many :tango_lists, dependent: :destroy
  
  #検索機能。index のところで呼び出される。検索ワードがなければaで始まるのを表示
  #tangoとtango_listコントローラで使う 
  def self.search(search)
    if search.length == 1 
      Tango.where("initial = ?", search)
    elsif search == "all"
      Tango.all
    else
      Tango.where("initial = ?", "a")
    end
  end
  
  
end
