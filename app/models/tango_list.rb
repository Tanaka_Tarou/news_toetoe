class TangoList < ApplicationRecord
  belongs_to :tango
  belongs_to :user
  
  def self.search(search)
    if search.length == 1 
      Tango.where("initial = ?", search)
    elsif search == "all"
      Tango.all
    else
      Tango.all
      #Tango.where("initial = ?", "a")
    end
  end
  
end
