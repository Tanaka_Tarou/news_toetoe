class Pick < ApplicationRecord
  has_many :likes
  belongs_to :user
  belongs_to :article
  validates :user_id, presence: true
  validates :article_id, presence: true
  validates :comment, length: {maximum: 2000}, allow_blank: true
  
  scope :mypick,     ->(current_user){where(user_id: current_user.id)}
  scope :followpick, ->(current_user){where(user_id: current_user.following)}
  scope :otherpick,  ->(current_user){where.not('user_id IN (?) OR user_id IN (?)',current_user.following.ids, current_user.id)}

end

