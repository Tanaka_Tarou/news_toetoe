json.array!(@tangos) do |tango|
  json.extract! tango, :id
  json.url tango_url(tango, format: :json)
end
