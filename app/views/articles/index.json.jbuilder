json.array!(@articles) do |article|
  json.extract! article, :id, :daiji_words, :title, :body, :sitename, :url
  json.url article_url(article, format: :json)
end
