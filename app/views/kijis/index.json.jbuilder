json.array!(@kijis) do |kiji|
  json.extract! kiji, :id, :daiji_words, :title, :body, :sitename, :url
  json.url kiji_url(kiji, format: :json)
end
