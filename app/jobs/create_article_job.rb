class CreateArticleJob < ApplicationJob
  require 'anemone'
  require 'csv'

  queue_as :default

  #拾ってくるだけではだめで、保存しなきゃだめだ。わすれとったので積んでた。
  #
  def perform(site_name)
    tmp = Article.new
    targets = tmp.get_target(site_name)
    targets.each do |ary|
      article = Article.new(ary)
      article.save
    end
  end
end
