# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170409050022) do

  create_table "articles", force: :cascade do |t|
    t.text     "daiji_words"
    t.text     "title"
    t.text     "body"
    t.text     "sitename"
    t.text     "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "tag"
    t.text     "pic"
    t.integer  "count"
    t.string   "site_name"
    t.datetime "post_time"
    t.string   "category"
    t.string   "shudopic"
    t.index ["id", "created_at"], name: "index_articles_on_id_and_created_at"
  end

  create_table "kijis", force: :cascade do |t|
    t.text     "daiji_words"
    t.text     "title"
    t.text     "body"
    t.text     "sitename"
    t.text     "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "pick_id"
    t.index ["pick_id"], name: "index_likes_on_pick_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "microposts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notices", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "read_flag",  default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "aiteno_id"
    t.string   "message"
    t.integer  "article_id"
    t.index ["user_id"], name: "index_notices_on_user_id"
  end

  create_table "picks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "article_id"
    t.string   "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id", "user_id"], name: "index_picks_on_article_id_and_user_id", unique: true
    t.index ["article_id"], name: "index_picks_on_article_id"
    t.index ["user_id"], name: "index_picks_on_user_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "samples", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tango_lists", force: :cascade do |t|
    t.integer  "tango_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tango_id"], name: "index_tango_lists_on_tango_id"
    t.index ["user_id", "tango_id"], name: "index_tango_lists_on_user_id_and_tango_id", unique: true
    t.index ["user_id"], name: "index_tango_lists_on_user_id"
  end

  create_table "tangos", force: :cascade do |t|
    t.string   "word"
    t.string   "meaning"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "initial"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "user_pic"
    t.text     "hitokoto"
    t.string   "remember_digest"
  end

end
