class CreateNotices < ActiveRecord::Migration[5.0]
  def change
    create_table :notices do |t|
      t.references :user, foreign_key: true
      t.references :article, foreign_key: true
      t.string :aiteno_id
      t.boolean :read_flag, default: false

      t.timestamps
    end
  end
end
