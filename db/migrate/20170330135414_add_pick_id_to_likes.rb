class AddPickIdToLikes < ActiveRecord::Migration[5.0]
  def change
    add_reference :likes, :pick, foreign_key: true
  end
end
