class CreateLikes < ActiveRecord::Migration[5.0]
  def change
    create_table :likes do |t|
      t.references :user, foreign_key: true
      t.references :pick_id, foreign_key: true

      t.timestamps
    end
  end
end
