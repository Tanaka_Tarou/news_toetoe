class AddSiteNameToArticles < ActiveRecord::Migration[5.0]
  def change
    add_column :articles, :site_name, :string
  end
end
