class CreateKijis < ActiveRecord::Migration[5.0]
  def change
    create_table :kijis do |t|
      t.text :daiji_words
      t.text :title
      t.text :body
      t.text :sitename
      t.text :url

      t.timestamps
    end
  end
end
