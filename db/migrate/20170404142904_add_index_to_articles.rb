class AddIndexToArticles < ActiveRecord::Migration[5.0]
  def change
    add_index :articles, [:id, :created_at]
  end
end
