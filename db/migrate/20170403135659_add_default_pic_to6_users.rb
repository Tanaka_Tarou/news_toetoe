class AddDefaultPicTo6Users < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :user_pic, :string, default: nil    
  end
end
