class AddDefaultPicToUsers < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :user_pic, :string, default: "0000.jpg"    
  end
end
