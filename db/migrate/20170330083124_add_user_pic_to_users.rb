class AddUserPicToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_pic, :string, default: "default.jpg"
  end
end
