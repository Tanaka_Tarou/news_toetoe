class AddIndexToTangoLists < ActiveRecord::Migration[5.0]
  def change
    add_index :tango_lists, [:user_id, :tango_id], :unique => true
  end
end
