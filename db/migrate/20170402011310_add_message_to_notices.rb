class AddMessageToNotices < ActiveRecord::Migration[5.0]
  def change
    add_column :notices, :message, :string
  end
end
