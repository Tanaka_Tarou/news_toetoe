class AddDefaultPicTo3Users < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :user_pic, :string, default: "private/uploads/user/default.jpg"    
  end
end
