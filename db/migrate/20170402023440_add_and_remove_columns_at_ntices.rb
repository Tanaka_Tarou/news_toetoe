class AddAndRemoveColumnsAtNtices < ActiveRecord::Migration[5.0]
  def change
    add_column    :notices, :article_id, :integer
    remove_column :notices, :article_title, :string
  end
end
