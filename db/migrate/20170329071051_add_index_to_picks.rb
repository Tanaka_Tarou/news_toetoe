class AddIndexToPicks < ActiveRecord::Migration[5.0]
  def change
    add_index :picks, [:article_id, :user_id], :unique => true    
  end
end
