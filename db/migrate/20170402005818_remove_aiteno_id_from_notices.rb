class RemoveAitenoIdFromNotices < ActiveRecord::Migration[5.0]
  def change
    remove_column :notices, :aiteno_id, :string
  end
end
