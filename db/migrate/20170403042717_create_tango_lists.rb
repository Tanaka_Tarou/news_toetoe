class CreateTangoLists < ActiveRecord::Migration[5.0]
  def change
    create_table :tango_lists do |t|
      t.references :tango, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
