class AddInitialToTangos < ActiveRecord::Migration[5.0]
  def change
    add_column :tangos, :initial, :string
  end
end
