class RemovePickIdIdFromLikes < ActiveRecord::Migration[5.0]
  def change
    remove_reference :likes, :pick_id, foreign_key: true
  end
end
