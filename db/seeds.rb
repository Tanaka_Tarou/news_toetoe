

User.create!(name:  "tarou tanaka",
             email: "foo@bar.com",
             password:              "password",
             password_confirmation: "password",
             admin: true)
             
99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n}@sample.com"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password )
end

#リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[2..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }


#記事
daiji_words = "{\"politician\"=>\"政治家\", \"political\"=>\"政治(上) の\","
sitename  = "Histroy"
post_time = "2017-03-29 09:28:22"
category  = "Culture"
title = "hmed Kathrada, Anti-Apartheid Activist and Mandel"
body  = "Anti-apartheid activist, South African politician ..."     
url   = "http://www.history.com/news/anti-apartheid-activist-and-mandela-confidant-ahmed-kathrada-dies" 
tag   = "[\"act\", \"advocate\", \"allow\", \"arrest\""
pic   = "1372648504759509471.jpeg"
count = 667

Article.create!(title: title,
                body:  body,
                url:   url,
                tag:   tag,
                pic:   pic,
                count: count,
                category: category,
                sitename: sitename,
                post_time: post_time,
                daiji_words: daiji_words)


#ピック
users[1..50].each do |usr|
  pick = usr.picks.build do |p|
    p.article_id = 1
    p.comment    = "#{usr.name}です。記念ピック"
  end
  pick.save
end

#ライクする
picks = Pick.all
users[2..50].each{|usr| user.likes.create!(pick_id: 1)}


#単語リスト作成
csv_data = CSV.read(Rails.root.join('app', 'assets', 'csvs','wanteds.csv'))
csv_data.each do |word|
  Tango.create!(word: word[0],
                meaning: word[1],
                initial: word[3])
end


